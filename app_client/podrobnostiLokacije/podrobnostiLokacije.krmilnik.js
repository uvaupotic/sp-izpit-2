(function() {
  /* global angular */
  
  function podrobnostiLokacijeCtrl($routeParams, $location, $uibModal, edugeocachePodatki, avtentikacija, geolokacija) {
    var vm = this;
    vm.idLokacije = $routeParams.idLokacije;
    
    vm.jePrijavljen = avtentikacija.jePrijavljen();
    
    vm.prvotnaStran = $location.path();
    
    vm.razdalja = 0;
    
    vm.pridobiPodatke = function(lokacija) {
      var lat = lokacija.coords.latitude,
          lng = lokacija.coords.longitude;
      vm.sporocilo = "Iščem bližnje lokacije.";
      edugeocachePodatki.razdalja(lat, lng, vm.idLokacije).then(
        function success(odgovor) {
          vm.sporocilo = odgovor.data.length > 0 ? "" : "Ne najdem lokacij.";
          vm.razdalja = odgovor.data[0].razdalja;
        }, 
        function error(odgovor) {
          vm.sporocilo = "Prišlo je do napake!";
          console.log(odgovor.e);
        });
    };
    
    vm.prikaziNapako = function(napaka) {
      
        vm.sporocilo = napaka.message;
      
    };
  
    vm.niLokacije = function() {
      
        vm.sporocilo = "Vaš brskalnik ne podpira geolociranja!";
      
    };
    
    vm.lokacija = geolokacija.vrniLokacijo(vm.pridobiPodatke, vm.prikaziNapako, vm.niLokacije);
    
    edugeocachePodatki.podrobnostiLokacijeZaId(vm.idLokacije).then(
      function success(odgovor) {
        vm.podatki = { lokacija: odgovor.data };
        vm.glavaStrani = {
          naslov: vm.podatki.lokacija.naziv
        };
      },
      function error(odgovor) {
        console.log(odgovor.e);
      }
    );
    
    vm.prikaziPojavnoOknoObrazca = function() {
      var primerekModalnegaOkna = $uibModal.open({
        templateUrl: '/komentarModalnoOkno/komentarModalnoOkno.pogled.html',
        controller: 'komentarModalnoOkno',
        controllerAs: 'vm',
        resolve: {
          podrobnostiLokacije: function() {
            return {
              idLokacije: vm.idLokacije,
              nazivLokacije: vm.podatki.lokacija.naziv,
              razdalja: vm.razdalja
            };
          }
        }
      });
      
      primerekModalnegaOkna.result.then(function(podatki) {
        if (typeof podatki != 'undefined')
          vm.podatki.lokacija.komentarji.push(podatki);
      }, function() {
        // Ulovi dogodek in ne naredi ničesar
      });
    };
  }
  podrobnostiLokacijeCtrl.$inject = ['$routeParams', '$location', '$uibModal', 'edugeocachePodatki', 'avtentikacija', 'geolokacija'];
  
  angular
    .module('edugeocache')
    .controller('podrobnostiLokacijeCtrl', podrobnostiLokacijeCtrl);
})();